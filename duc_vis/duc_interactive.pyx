
cdef extern from "duc.h":
	# int duc_open(duc *duc, const char *path_db, duc_open_flags flags)

	struct duc:
		pass

	enum duc_open_flags:
		DUC_OPEN_RO

	enum duc_errno:
		DUC_OK

	# Top-level file/database
	duc* duc_new()
	void duc_del(duc* duc)
	int duc_open(duc* duc, const char* path_db, duc_open_flags flags)
	int duc_close(duc* duc)
	char* duc_strerror(duc* duc)

	# Index report (show which directory we have scanned)
	struct duc_index_report:
		char* path
		
	duc_index_report* duc_get_report(duc* duc, size_t id)
	int duc_index_report_free(duc_index_report* rep)

	# File type
	enum duc_file_type:
		pass

	char duc_file_type_char(duc_file_type t)

	# List dir
	struct duc_size:
		size_t actual
		size_t apparent
		size_t count

	struct duc_dir:
		pass
	
	duc_dir* duc_dir_open(duc *duc, const char *path)
	int duc_dir_close(duc_dir* dir)

	void duc_dir_get_size(duc_dir *dir, duc_size *size)

	struct duc_dirent:
		char* name
		duc_file_type type
		duc_size size

	enum duc_sort:
		DUC_SORT_SIZE
		DUC_SORT_NAME
	
	enum duc_size_type:
		DUC_SIZE_TYPE_APPARENT,
		DUC_SIZE_TYPE_ACTUAL
		DUC_SIZE_TYPE_COUNT

	duc_dirent* duc_dir_read(duc_dir* dir, duc_size_type st, duc_sort sort)




from pathlib import Path
import logging
log = logging.getLogger('duc.bridge')

cdef size_info_to_dict(sz : duc_size):
	return dict(size = sz.actual, num_files = sz.count)


cdef class DucDatabase:
	""" DucDatabase('scan_file.duc.db') """

	cdef duc* duc_handle

	def __cinit__(self):
		self.duc_handle = duc_new()
	
	def __dealloc__(self):
		if self.duc_handle is not NULL:
			duc_del(self.duc_handle)

	def __init__(self, path):
		self.open(path)

	def open(self, path):
		path = Path(path)
		if not path.is_file():
			raise FileNotFoundError(f'{path} does not exist or is not a file')
		
		cdef bytes path_bytes = bytes(path)

		cdef int open_result = duc_open(self.duc_handle, path_bytes, duc_open_flags.DUC_OPEN_RO)

		if open_result != duc_errno.DUC_OK:
			log.warning(f'duc open result is {open_result}')
			error_msg = duc_strerror(self.duc_handle)

			raise RuntimeError(f'Error in duc_open: {error_msg}')

	def info(self):
		cdef duc_index_report* report = duc_get_report(self.duc_handle, 0)

		if report is NULL:
			raise RuntimeError('duc_get_report(handle, 0) returned null')

		cdef bytes report_path = report.path

		duc_index_report_free(report)

		return report_path

	def size(self, path):
		cdef path_bytes = bytes(path)
		cdef duc_dir* dir_handle = duc_dir_open(self.duc_handle, path_bytes)
		if dir_handle is NULL:
			raise ValueError(f'duc_dir_open returned null for path {path}')

		cdef duc_size size_info
		duc_dir_get_size(dir_handle, &size_info)
		size_info_py = dict(size = size_info.actual, num_files = size_info.count)
		
		duc_dir_close(dir_handle)

		return size_info_py

	def ls(self, path):
		cdef path_bytes = bytes(path)
		cdef duc_dir* dir_handle = duc_dir_open(self.duc_handle, path_bytes)
		if dir_handle is NULL:
			raise ValueError(f'duc_dir_open returned null for path {path}')

		cdef duc_size size_info
		duc_dir_get_size(dir_handle, &size_info)
		size_info_py = size_info_to_dict(size_info)
		
		cdef duc_dirent* child_info = duc_dir_read(dir_handle, duc_size_type.DUC_SIZE_TYPE_ACTUAL, duc_sort.DUC_SORT_SIZE)

		child_infos = []

		while child_info is not NULL:
			child_info_py = size_info_to_dict(child_info.size)
			child_info_py['name'] = child_info.name
			child_info_py['type'] = chr(duc_file_type_char(child_info.type))
			child_infos.append(child_info_py)

			child_info = duc_dir_read(dir_handle, duc_size_type.DUC_SIZE_TYPE_ACTUAL, duc_sort.DUC_SORT_SIZE)


		size_info_py['children'] = child_infos

		duc_dir_close(dir_handle)

		return size_info_py


def say_hello():
	print('Module build')
