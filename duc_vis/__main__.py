from .duc_interactive import DucDatabase
from pathlib import Path
import logging
import click

log = logging.getLogger('duc.cli')

@click.command()
@click.argument('db_path', type=click.Path(file_okay=True, dir_okay=False, exists=True))
def main(db_path):
	db_path = Path(db_path)
	log.info(f'Trying to open {db_path}')
	db = DucDatabase(db_path)

	log.info(f'info: {db.info()}')

if __name__ == '__main__':
	main()
