from setuptools import find_packages
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
from pathlib import Path

DUC_LIB_SRC_DIR = Path('../duc/src/libduc')
DUC_LIB_SOURCES = list(DUC_LIB_SRC_DIR.glob('*.c'))

def paths_to_str(paths):
	return [str(p) for p in paths]

extensions = [
	Extension("duc_vis.duc_interactive",
		paths_to_str(DUC_LIB_SOURCES + ["duc_vis/duc_interactive.pyx"]),
		include_dirs = paths_to_str(['/usr/include', DUC_LIB_SRC_DIR]),
		libraries = ['tokyocabinet'],
		# library_dirs=[...]
	),
]
setup(
	name="duc_vis",
	ext_modules=cythonize(extensions, language_level = "3"),
) 
